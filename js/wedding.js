var db = firebase.firestore();

function addAttendee(form) {

console.log("form.firstname");
console.log(form.firstname.value);
	db.collection("attendees").add({
		firstName: form.firstname.value,
		lastName: form.lastname.value,
		phoneNumber: form.phonenumber.value,
		email: form.email.value,
		allergies: form.allergies.value,
		attending: form.attending.value,
		message: form.message.value
	})
	.then(function(docRef) {
		console.log("Document written with ID: ", docRef.id);
	})
	.catch(function(error) {
		console.error("Error adding document: ", error);
	});
	document.getElementById('id01').style.display='none';
}
function addNonAttendees(form) {
	var nameList = form.names.value.split('\n');
	for (i = 0; i < nameList.length; i++) { 
		addNonAttendee(nameList[i]);
	}
	document.getElementById('id01').style.display='none';
}
function addNonAttendee(name) {
    var names = name.split(' ');
	db.collection("nonattendees").add({
		first: names[0],
		last: names[names.length -1]
	})
	.then(function(docRef) {
		console.log("Document written with ID: ", docRef.id);
	})
	.catch(function(error) {
		console.error("Error adding document: ", error);
	});
}
// Counter
function count() {
	$(document).ready(function() {

	  var counters = $(".count");
	  var countersQuantity = counters.length;
	  var counter = [];

	  for (i = 0; i < countersQuantity; i++) {
		counter[i] = parseInt(counters[i].innerHTML);
	  }

	  var count = function(start, value, id) {
		var localStart = start;
		setInterval(function() {
		  if (localStart < value) {
			localStart++;
			counters[id].innerHTML = localStart;
		  }
		}, 40);
	  }

	  for (j = 0; j < countersQuantity; j++) {
		count(0, counter[j], j);
	  }
	});
}